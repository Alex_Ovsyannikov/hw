<?php
$animals = [
    'Africa' => ['Elephantidae', 'Panthera pardus', 'Pan troglodytes'],
    'South America' => ['Hydrochoerus hydrochaeris', 'Marmosops incanus', 'Bradypus variegatus'],
    'North America' => ['Procyon lotor', 'Canis latrans', 'Ursus arctos horribilis'],
    'Eurasia' => ['Canis lupus', 'Vulpes vulpes', 'Sciurus'],
    'Australia' => ['Macropus rufus', 'Vombatidae', 'Myrmecobius fasciatus'],
];

$two_words = [];
$first_word = [];
$sec_word = [];
$fantastic_animals = [];

foreach ($animals as $continent => $anim) {
    foreach ($anim as $animal) {
        if (str_word_count($animal)==2) {
            $two_words[$continent][] = $animal;
            $words = explode(' ', $animal);
            $first_word[$continent][] = $words[0];
            $sec_word[] = $words[1];
        }
    }
}

shuffle($sec_word);

$i = 0;
foreach ($first_word as $continent => $word) {
    foreach ($word as $animal) {
        $fantastic_animals[$continent][] = $animal.' '.$sec_word[$i];
        $i++;
    }
}

foreach ($fantastic_animals as $continent => $anim) {
    $animals_out = implode(',', $anim);
    echo "<h2>$continent</h2><p>$animals_out</p>";
}

?>